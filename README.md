# 12V lead acid battery charger

12V 1.5A Trickle LEAD ACID BATTERY CHARGER
MODEL: SW2012
Type:	Lead-Acid Battery
Charger Type:	Lead-Acid Battery Charger
Charger Application:	Electric Toy/Tools/Light/Lamp, Military Station
Nominal Voltage:	13.8V